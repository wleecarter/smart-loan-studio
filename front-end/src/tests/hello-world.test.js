const greeting = require('../hello-world');

describe('greeting()', () => {
  it('says hello', () => {
    expect(greeting('World')).toBe('Hello, World!');
  });
});
