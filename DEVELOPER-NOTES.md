# project setup

## the basics

- `git clone https://gitlab.com/wleecarter/smart-loan-studio.git`
- add .gitignore file
  - exclude node_modules, .vscode
- `npm init -y` (to add package.json)
- add jsconfig.json

## eslint

- `npm​​ ​​install​​ ​​--save-dev​​ ​​eslint`
- `npx eslint --init` (to add .eslintrc.js)

## prettier

- `npm install --save-dev prettier-eslint-cli`

## jest'

- `npm​​ ​​install​​ ​​--save-dev​​ ​​jest`
- update jsconfig.json to jest in vscode intellisense - https://humanwhocodes.com/snippets/2019/05/jest-globals-intellisense-visual-studio-code/
- `npm install --save-dev eslint-plugin-jest`
- add .eslintrc with to tests directory for jest-specific settings
